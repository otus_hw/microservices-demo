## подготовка MVP инфраструктурной платформы для приложения-примера

Для развертывания выбрано мультикомпонентное приложение от Google [Online Boutique](https://gitlab.com/otus_hw/microservices-source).

## Требования к проектной работе:

- [x] Автоматизация создания и настройки кластера
- [x] Развертывание и настройка сервисов платформы
    - [x]  Инфраструктура для CI/CD - Gitlab, Flux  
    - [x]  Инфраструктура для сбора обратной связи - Prometheus, Grafana, Loki, Yager
- [x] Настройка мониторинга, алерты, дашборды
- [x] CI/CD пайплайн для приложения описан кодом
- [x] Все, что имеет отношение к проекту хранится в Git  
- [x] Документация 
    - [x] -  README с описанием решения
    - [x] -  CHANGELOG с описанием выполненной работы
    - [x] -  how_to_start.md
    - [x] -  how_to_build_image.md  
- [x] Screencasts
    - [x] - create_dev_k8s_cluster.avi 
    - [x] - deploy_platform.avi            

## Концеация проектной работы

- [x] Создание managed мульти-кластера (GCP+Istio), код в terraform
   - [x] Staging
   - [x] Production 
- [x] Скрипты для развертывания Prometheus, Grafana, Flux, Loki, Yager, код в helmfile
- [x] GitLab - SaaS
- [x] Скрипты для развертывания приложения

## Инфраструктура

- В качестве инфраструктуры выбран облачный провайдер Google Cloud

## Платформа

- В качестве платформы выбран kubernetes 1.18.16-gke.302. Istio 1.9

## Используемое в pipeline программное обеспечение

# Разверывание kubernetes-кластера 
    - terraform
# Установка сервисов в кластер 
    - helm3
    - helmfile
# Организация процесса СI
    - GitLab pipeline
# Организация процесса CD
    - flux
    - flagger
    - fluxctl    
# Управление кластером
    - kubectl
# Управление облачной инфраструктурой
    - google cloud sdk

## Система хранения версий для инфраструктурного кода
    - [https://gitlab.com/otus_hw/microservices-demo
## Система хранения версий для приложения
    - https://gitlab.com/otus_hw/microservices-source
# Документация по проекту
    - https://gitlab.com/otus_hw/microservices-demo/-/tree/master/docs

# Реестр docker-образов инфраструктуры
    - Gitlab docker container registry

# Реестр docker-образов приложения  
    - Docker Hub

# Использованы docker-образы
    - google/cloud-sdk:334.0.0-alpine
    - docker:20.10-git
    - docker:20.10-dind
    - hashicorp/terraform:light
        ]
