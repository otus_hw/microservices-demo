# Развертывание k8s-кластера DEV

По нажатию кнопки старт в scheduler стартует двух-шаговый pipeline:

![](docs/pipeline.png)

**Переменные для pipeline:**

![](docs/variables.png)


1. стадия **deploy-k8s-cluster**
   - джоб deploy-infra_dev
   - поднимает managed k8s кластер OMEGA в google cloud
   - требует задать переменную INFRA_DEV="true"
![](run_pipeline_dev.png)

2. стадия **deploy-platform**
   - джоб deploy-platform-dev
   - требует задать переменную INFRA_DEV="true"
   - устанавливает все необходимое программное обеспечение:
      - helm3 
      - helmfile
      - kubectl
      - flux
      - google cloud sdk
      - flagger
      - Istio
   - поднимает в кластере средства для мониторинга, визуализации и логгирования
      - prometheus
      - grafana
      - loki
   - создает helm-operator
   - открывает доступ к prometheus и grafana по внешнему адресу 
   - подтягивает datasource Prometheus и Loki
   - подтягивает кастомные и встроенные дашборды 

3. стадия **destroy-k8s-cluster**
   - джоб destroy-infra_dev
   - удаляет managed k8s кластер OMEGA в google cloud
   - требует задать переменную INFRA_DEV="true"
   

Во время работы джоба **deploy-platform** в консоли со своей локалной машины:

~~~sh
$ fluxctl identity --k8s-fwd-ns flux
ssh-rsa AAAAB3Nza......c5jRXrrxs= root@flux-6fc948c975-jck2g
~~~

Добавляем ключ в Gitlab.

После того джоб отработал - flux синхронизирует состояние кластера с Gitlab репозиторием и с помощью helm-оператора.

Кластер подимается. Приложение доступно.  

![cluster_ready](docs/cluster_ready.png)

![heapster-shop](heapster-shop.png)


Сразу после того как приложение развернуто в графане можем смотреть дашборды:

![dashboard](grafana_dashboard.png)

![запрос к ментрикам Prometheus](grafana_dashboard_details.png)

![переменные дашборда](grafana_dashboard_details_vars.png)

# Развертывание k8s-кластера PROD

1. стадия **deploy-k8s-cluster**
   - джоб deploy-infra_prod
   - поднимает managed k8s кластер ladoga в google cloud
   - требует задать переменную INFRA_PROD="true"
![](run_pipeline.png)

2. стадия **deploy-platform**
   - джоб deploy-platform-prod
   - требует задать переменную INFRA_PROD="true"
3. стадия **destroy-k8s-cluster**
   - джоб destroy-infra_PROD
   - удаляет managed k8s кластер ladoga в google cloud
   - требует задать переменную INFRA_PROD="true
   