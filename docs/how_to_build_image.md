# Contious integration

Непрерывная поставка обеспечивается автоматическим запуском pipeline в проекте [microservices-source](https://gitlab.com/otus_hw/microservices-source)

Джоб запускается при коммите в папку $CI_PIPELINE_SOURCE:

    rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'    
      changes:    
        - $SOURCE_DIR/* 
      when: always

**Джоб запускает Makefile, который строит образ микросервиса и пушит его на Docker Hub:**

![](build_pipeline.png)

**Pipeline использует следующие переменные**

![](build_pipeline_vars.png)

Flux видит изменения в репозитории докер образов и меняет номер версии в HelmRelease (deploy/releases).

На изменения микросервиса frontend реагирует flagger и запускает "канарейку" на новую версию релиза:


<details>
  <summary> Выкатка нового релиза (запускаем канарейку) </summary>

~~~sh
$ kubectl describe canary frontend -n microservices-demo
Name:         frontend
Namespace:    microservices-demo
Labels:       <none>
Annotations:  helm.fluxcd.io/antecedent: microservices-demo:helmrelease/frontend-hipster
API Version:  flagger.app/v1beta1
Kind:         Canary
Metadata:
  Creation Timestamp:  2021-04-19T15:16:34Z
  Generation:          1
  Managed Fields:
    API Version:  flagger.app/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:spec:
        .:
        f:analysis:
          .:
          f:interval:
          f:maxWeight:
          f:metrics:
          f:stepWeight:
          f:threshold:
        f:progressDeadlineSeconds:
        f:provider:
        f:service:
          .:
          f:gateways:
          f:hosts:
          f:port:
          f:targetPort:
          f:trafficPolicy:
            .:
            f:tls:
              .:
              f:mode:
        f:targetRef:
          .:
          f:apiVersion:
          f:kind:
          f:name:
    Manager:      Go-http-client
    Operation:    Update
    Time:         2021-04-19T15:16:34Z
    API Version:  flagger.app/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:helm.fluxcd.io/antecedent:
    Manager:      kubectl
    Operation:    Update
    Time:         2021-04-19T15:16:35Z
    API Version:  flagger.app/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:spec:
        f:service:
          f:portDiscovery:
      f:status:
        .:
        f:canaryWeight:
        f:conditions:
        f:failedChecks:
        f:iterations:
        f:lastAppliedSpec:
        f:lastTransitionTime:
        f:phase:
        f:trackedConfigs:
    Manager:         flagger
    Operation:       Update
    Time:            2021-04-19T15:17:07Z
  Resource Version:  23742
  Self Link:         /apis/flagger.app/v1beta1/namespaces/microservices-demo/canaries/frontend
  UID:               e26e1c9e-43d9-46b5-93c1-eecb38046668
Spec:
  Analysis:
    Interval:    30s
    Max Weight:  30
    Metrics:
      Interval:               30s
      Name:                   request-success-rate
      Threshold:              99
    Step Weight:              5
    Threshold:                5
  Progress Deadline Seconds:  60
  Provider:                   istio
  Service:
    Gateways:
      frontend-gateway
    Hosts:
      *
    Port:         80
    Target Port:  8080
    Traffic Policy:
      Tls:
        Mode:  DISABLE
  Target Ref:
    API Version:  apps/v1
    Kind:         Deployment
    Name:         frontend
Status:
  Canary Weight:  0
  Conditions:
    Last Transition Time:  2021-04-19T15:56:07Z
    Last Update Time:      2021-04-19T15:56:07Z
    Message:               Canary analysis completed successfully, promotion finished.
    Reason:                Succeeded
    Status:                True
    Type:                  Promoted
  Failed Checks:           0
  Iterations:              0
  Last Applied Spec:       589ddf7f99
  Last Transition Time:    2021-04-19T15:56:07Z
  Phase:                   Succeeded
  Tracked Configs:
Events:
  Type     Reason  Age                   From     Message
  ----     ------  ----                  ----     -------
  Warning  Synced  45m                   flagger  frontend-primary.microservices-demo not ready: waiting for rollout to finish: observed deployment generation less then desired generation
  Normal   Synced  44m (x2 over 45m)     flagger  all the metrics providers are available!
  Normal   Synced  44m                   flagger  Initialization done! frontend.microservices-demo
  Normal   Synced  10m                   flagger  New revision detected! Scaling up frontend.microservices-demo
  Normal   Synced  9m38s                 flagger  Starting canary analysis for frontend.microservices-demo
  Normal   Synced  9m38s                 flagger  Advance frontend.microservices-demo canary weight 5
  Normal   Synced  9m8s                  flagger  Advance frontend.microservices-demo canary weight 10
  Normal   Synced  8m38s                 flagger  Advance frontend.microservices-demo canary weight 15
  Normal   Synced  8m8s                  flagger  Advance frontend.microservices-demo canary weight 20
  Normal   Synced  7m38s                 flagger  Advance frontend.microservices-demo canary weight 25
  Normal   Synced  7m8s                  flagger  Advance frontend.microservices-demo canary weight 30
  Normal   Synced  6m38s                 flagger  Copying frontend.microservices-demo template spec to frontend-primary.microservices-demo
  Normal   Synced  5m38s (x2 over 6m8s)  flagger  (combined from similar events): Promotion completed! Scaling down frontend.microservices-demo
~~~

</details>





